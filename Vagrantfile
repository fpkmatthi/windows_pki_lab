require 'rbconfig'
require 'yaml'

# set default LC_ALL for all BOXES
ENV["LC_ALL"] = "en_GB.UTF-8"

# Set your default base box here
DEFAULT_BASE_BOX = 'bento/centos-7.5'

# When set to `true`, Ansible will be forced to be run locally on the VM
# instead of from the host machine (provided Ansible is installed).
FORCE_LOCAL_RUN = false

VAGRANTFILE_API_VERSION = '2'
PROJECT_NAME = '/' + File.basename(Dir.getwd)

# set custom vagrant-hosts file
vagranthosts = ENV['VAGRANTS_HOST'] ? ENV['VAGRANTS_HOST'] : 'vagrant-hosts.yml'
hosts = YAML.load_file(File.join(__dir__, vagranthosts))

# {{{ Helper functions

# Set options for the network interface configuration. All values are
# optional, and can include:
# - ip (default = DHCP)
# - netmask (default value = 255.255.255.0
# - mac
# - auto_config (if false, Vagrant will not configure this network interface
# - intnet (if true, an internal network adapter will be created instead of a
#   host-only adapter)
def network_options(network)
  options = {}

  if network.key?('ip')
    options[:ip] = network['ip']
    options[:netmask] = network['netmask'] ||= '255.255.255.0'
  else
    options[:type] = 'dhcp'
  end

  options[:mac] = network['mac'].gsub(/[-:]/, '') if network.key?('mac')
  options[:auto_config] = network['auto_config'] if network.key?('auto_config')
  options[:virtualbox__intnet] = true if network.key?('intnet') && network['intnet']
  options   
end

def custom_synced_folders(vm, host)
  return unless host.key?('synced_folders')
  folders = host['synced_folders']

  folders.each do |folder|
    vm.synced_folder folder['src'], folder['dest'], folder['options']
  end
end
# }}}

# Set options for shell provisioners to be run always. If you choose to include {{{
# it you have to add a cmd variable with the command as data.
#
# Use case: start symfony dev-server
#
# example:
# shell_always:
#   - cmd: php /srv/google-dev/bin/console server:start 192.168.52.25:8080 --force
def shell_provisioners_always(vm, host)
  if host.has_key?('shell_always')
    scripts = host['shell_always']

    scripts.each do |script|
      vm.provision "shell", inline: script['cmd'], run: "always"
    end
  end
end

# }}}

# Adds forwarded ports to your Vagrant machine {{{
#
# example:
#  forwarded_ports:
#    - guest: 88
#      host: 8080
def forwarded_ports(vm, host)
  if host.has_key?('forwarded_ports')
    ports = host['forwarded_ports']

    ports.each do |port|
      vm.network "forwarded_port", guest: port['guest'], host: port['host']
    end
  end
end

# }}}

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  hosts.each do |host|
    # config.ssh.private_key_path = [".ssh/id_rsa", "~/.vagrant.d/insecure_private_key"]
    config.vm.define host['name'] do |node|
      # General stuff {{{
      node.vm.box = host['box'] ||= DEFAULT_BASE_BOX
      node.vm.box_url = host['box_url'] if host.key? 'box_url'
      node.vm.box_version = host['version']
      # node.disksize.size = '50GB'

      node.vm.hostname = host['name']
      # if host.key?('networks')
        # networks = host['networks']  
        # networks.each do |network|
        # node.vm.network :private_network, network_options(network)
      node.vm.network :private_network, network_options(host)
        # end
      # end
      custom_synced_folders(node.vm, host)
      shell_provisioners_always(node.vm, host)
      forwarded_ports(node.vm, host)
      # }}}

      # Virtualbox {{{
      node.vm.provider :virtualbox do |vb, override|
        vb.memory = host['memory'] if host.key? 'memory'
        vb.cpus = host['cpus'] if host.key? 'cpus'
        vb.gui = host['gui'] if host.key? 'gui'

        # WARNING: if the name of the current directory is the same as the
        # host name, this will fail.
        vb.customize ['modifyvm', :id, '--groups', PROJECT_NAME]

        # GPU settings
        vb.customize ['modifyvm', :id, '--vram', '128']
        vb.customize ['modifyvm', :id, '--accelerate3d', 'on']
      end
      # }}}
      
      # config.winrm.transport       = :plaintext
      # config.winrm.basic_auth_only = true
      config.winrm.timeout         = 3600
      config.vm.boot_timeout       = 3600
      
      # Shell provisioning {{{
      provisioning_files = './provisioning'
      if host['name'] == 'win-dc01'
        node.winrm.transport       = :plaintext
        node.winrm.basic_auth_only = true
        node.vm.provision 'shell',
          privileged: true,
          path: provisioning_files + '/general.ps1',
          args: [
            host['name'],
            host['ip'],
            host['lan_prefix'],
            'WAN',
            'LAN',
            host['default_gateway'],
            host['primary_dns'],
            host['secondary_dns'],
            host['country'],
            host['timezone'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision 'shell',
          privileged: true,
          path: provisioning_files + '/win-dc01/dc_install.ps1',
          args: [
            host['name'],
            host['domain'],
            host['netbios'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision :reload
        node.vm.provision 'shell',
          privileged: true,
          path: provisioning_files + '/win-dc01/dc_config.ps1',
          args: [
            host['name'],
            'WAN',
            'LAN',
            host['primary_dns'],
            host['secondary_dns'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision 'shell',
          privileged: true,
          path: provisioning_files + '/win-dc01/dns_config.ps1',
          args: [
            host['name'],
            host['domain'],
            host['ip_win_root_ca'],
            host['ip_win_issuing_ca']
          ]
        node.vm.provision 'shell',
          privileged: true,
          path: provisioning_files + '/win-dc01/dhcp_install_config.ps1',
          args: [
            host['name'],
            host['domain'],
            host['ip'],
            host['default_gateway'],
            host['primary_dns'],
            host['secondary_dns'],
            host['dhcp_start_range'],
            host['dhcp_end_range']
          ]
        node.vm.provision :reload
        ### NOTE: install NAT via gui (!re-enable adapters after install and before activation of NAT)
      elsif host['name'] == 'win-webserv01'
        node.winrm.transport       = :plaintext
        node.winrm.basic_auth_only = true
        node.vm.provision 'shell',
          privileged: true,
          run: 'always',
          path: provisioning_files + '/general.ps1',
          args: [
            host['name'],
            host['ip'],
            host['lan_prefix'],
            'WAN',
            'LAN',
            host['default_gateway'],
            host['primary_dns'],
            host['secondary_dns'],
            host['country'],
            host['timezone'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision 'shell',
          privileged: true,
          path: provisioning_files + '/join_domain.ps1',
          args: [
            host['name'],
            host['domain'],
            host['netbios'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision :reload
        node.vm.provision 'shell',
          privileged: true,
          path: provisioning_files + '/win-webserv01/iis_install_config.ps1',
          args: [
            host['name'],
            host['domain'],
            host['netbios'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision :reload
      elsif host['name'] == 'win-root-ca'
        node.winrm.transport       = :plaintext
        node.winrm.basic_auth_only = true
        node.vm.provision 'shell',
          privileged: true,
          run: 'always',
          path: provisioning_files + '/general.ps1',
          args: [
            host['name'],
            host['ip'],
            host['lan_prefix'],
            'WAN',
            'LAN',
            host['default_gateway'],
            host['primary_dns'],
            host['secondary_dns'],
            host['country'],
            host['timezone'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision 'shell',
          privileged: true,
          path: provisioning_files + '/win-root-ca/ca_install.ps1',
          args: [
            host['name'],
            host['domain'],
            host['netbios'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        # node.vm.provision :reload
      elsif host['name'] == 'win-issuing-ca'
        node.winrm.transport       = :plaintext
        node.winrm.basic_auth_only = true
        node.vm.provision 'shell',
          privileged: true,
          run: 'always',
          path: provisioning_files + '/general.ps1',
          args: [
            host['name'],
            host['ip'],
            host['lan_prefix'],
            'WAN',
            'LAN',
            host['default_gateway'],
            host['primary_dns'],
            host['secondary_dns'],
            host['country'],
            host['timezone'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision 'shell', 
          privileged: true, 
          path: provisioning_files + '/join_domain.ps1', 
          args: [
            host['name'],
            host['domain'],
            host['netbios'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision :reload
        node.vm.provision 'shell',
          privileged: true,
          path: provisioning_files + '/win-issuing-ca/issuing_config.ps1',
          args: [
            host['name'],
            host['domain'],
            host['netbios'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        # node.vm.provision :reload
      elsif host['name'] == 'win-clt01'
        # node.winrm.transport       = :plaintext
        # node.winrm.basic_auth_only = false
        node.vm.provision 'shell', 
          privileged: true, 
          path: provisioning_files + '/general.ps1', 
          args: [
            host['name'],
            host['ip'],
            host['lan_prefix'],
            'WAN',
            'LAN',
            host['default_gateway'],
            host['primary_dns'],
            host['secondary_dns'],
            host['country'],
            host['timezone'],
            host['local_admin_pw'],
            host['domain_admin_pw']
          ]
        node.vm.provision :reload
        node.vm.provision 'shell', 
          privileged: true, 
          path: provisioning_files + '/win-clt01/user_join_domain.ps1', 
          args: [
            host['name'],
            host['domain'],
            host['domain_user'],
            host['domain_user_pw']
          ]
        node.vm.provision :reload
        node.vm.provision 'shell', 
          privileged: true, 
          run: 'always',
          path: provisioning_files + '/win-clt01/fix_adapter_settings.ps1', 
          args: [
            host['name'],
            'LAN'
          ]
      end
      # }}}
    end
  end
end

# -*- mode: ruby -*-
# vi: ft=ruby :
