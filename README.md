# Windows PKI lab (Two Tier Model)

![Two tier model](images/topology.png)

## Steps

1. vagrant up win-dc01
2. manually install NAT routing on win-dc01
3. vagrant up win-root-ca
4. vagrant up win-webserv01
5. vagrant up win-issuing-ca
6. manually run ca_submit_req.ps1 on win-root-ca
7. issue pending request via CA MMC
8. manually run ca_retrieve_crt.ps1 on win-root-ca
9. manually run issuing_post_config.ps1 on win-issuing-ca
10. verify PKI health (guide 1, p.18)
11. vagrant up win-clt01
12. configure gpo for auto enrollment (guide 1, p.19-20)

![Workstation authentication template certificate](images/ws_auth_templ_crt.png)

![Auto-enrolled workstation certificate](images/auto_enrolled_ws_crt.png)


## Resources

### The ones I found most useful
- https://sec.ch9.ms/sessions/teched/eu/2014/Labs/EM-H309.pdf
- https://www.rebeladmin.com/2018/06/step-step-guide-setup-two-tier-pki-environment/

### Other
- [Microsoft docs pt1](https://docs.microsoft.com/nl-nl/archive/blogs/yungchou/enterprise-pki-with-windows-server-2012-r2-active-directory-certificate-services-part-1-of-2)
- [Microsoft docs pt2](https://docs.microsoft.com/nl-nl/archive/blogs/yungchou/enterprise-pki-with-windows-server-2012-r2-active-directory-certificate-services-part-2-of-2)
