# Provisioning script for win-root-ca

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname        = 'win-root-ca',
    [string]$domain          = 'fpkmatthi.local',
    [string]$netbios         = 'FPKMATTHI',
    [string]$local_admin_pw  = 'LocalAdmin2019',
    [string]$domain_admin_pw = 'DomainAdmin2019'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"
$secure_local_admin_pw  = ConvertTo-SecureString $local_admin_pw -AsPlainText -Force
$secure_domain_admin_pw = ConvertTo-SecureString $domain_admin_pw -AsPlainText -Force

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting Request submittal on $hostname"

$creds = New-Object System.Management.Automation.PsCredential("$netbios\Administrator",$secure_domain_admin_pw) 

certreq -retrieve 2 "C:\win-issuing-ca.fpkmatthi.local_fpkmatthi_Issuing CA.crt"

New-PSDrive -Name winissuingca -PSProvider FileSystem -Root '\\WIN-ISSUING-CA\C$' -Credential $creds | ForEach-Object { Set-Location "$_`:" }
Copy "C:\win-issuing-ca.fpkmatthi.local_fpkmatthi_Issuing CA.crt" ./

