# Provisioning script for win-root-ca

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname        = 'win-root-ca',
    [string]$domain          = 'fpkmatthi.local',
    [string]$netbios         = 'FPKMATTHI',
    [string]$local_admin_pw  = 'LocalAdmin2019',
    [string]$domain_admin_pw = 'DomainAdmin2019'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"
$secure_local_admin_pw  = ConvertTo-SecureString $local_admin_pw -AsPlainText -Force
$secure_domain_admin_pw = ConvertTo-SecureString $domain_admin_pw -AsPlainText -Force

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting Request submittal on $hostname"

$creds = New-Object System.Management.Automation.PsCredential("$netbios\Administrator",$secure_domain_admin_pw) 

# ---
# terminal style: https://serverfault.com/questions/517261/is-it-possible-to-batch-sign-csrs-with-certutil-for-instance
# TODO: get id form output
# $reqid = certreq -submit "C:\win-issuing-ca.fpkmatthi.local_fpkmatthi Issuing CA.req" | Select-String -Pattern 'RequestId' -CaseSensitive -SimpleMatch | Select-Object -First 1
# $reqid.Split(":")
# certutil -resubmit <id from output>
# certreq -Retrieve <id from output> "C:\win-issuing-ca.fpkmatthi.local_fpkmatthi_Issuing CA.crt"
# ---

certreq -submit "C:\win-issuing-ca.fpkmatthi.local_fpkmatthi Issuing CA.req"
# TODO: issue request via gui
# Server Manager > CA > pending requests > RMB > issue
