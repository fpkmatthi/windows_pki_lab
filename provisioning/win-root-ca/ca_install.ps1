# Provisioning script for win-root-ca

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname        = 'win-root-ca',
    [string]$domain          = 'fpkmatthi.local',
    [string]$netbios         = 'FPKMATTHI',
    [string]$local_admin_pw  = 'LocalAdmin2019',
    [string]$domain_admin_pw = 'DomainAdmin2019'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"
$secure_local_admin_pw  = ConvertTo-SecureString $local_admin_pw -AsPlainText -Force
$secure_domain_admin_pw = ConvertTo-SecureString $domain_admin_pw -AsPlainText -Force

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting domain controller installation on $hostname"

# install ca stuff
info "Installing ADCS cert authority"
Install-WindowsFeature ADCS-Cert-Authority -IncludeManagementTools
Install-ADcsCertificationAuthority -CACommonName "fpkmatthi Root CA" -CAType StandaloneRootCA -CryptoProviderName "RSA#Microsoft Software Key Storage Provider" -HashAlgorithmName SHA256 -KeyLength 2048 -ValidityPeriod Years -ValidityPeriodUnits 20 -Force

# config cdp etc to keep on DC
info "Configuring CDP to store on DC"
certutil -setreg ca\DSConfigDN "CN=Configuration,DC=fpkmatthi,DC=local"
certutil -setreg ca\DSConfig "fpkmatthi.local"
Certutil -setreg CA\AuditFilter 127 
# certutil -getreg ca\CRLPublicationURLs

$crllist = Get-CACrlDistributionPoint;
foreach ($crl in $crllist) {
    Remove-CACrlDistributionPoint $crl.uri -Force
}

info "Adding CRL distribution point"
certutil -setreg CA\CRLPublicationURLs "1:C:\Windows\system32\CertSrv\CertEnroll\%3%8%9.crl\n10:ldap:///CN=%7%8,CN=%2,CN=CDP,CN=Public Key Services,CN=Services,%6%10\n2:http://crt.fpkmatthi.local/CertEnroll/%3%8%9.crl"
 
 info "Adding AIA distribution point"
 certutil -setreg CA\CACertPublicationURLs "1:C:\Windows\system32\CertSrv\CertEnroll\%1_%3%4.crt\n2:ldap:///CN=%7,CN=AIA,CN=Public Key Services,CN=Services,%6%11\n2:http://crt.fpkmatthi.local/CertEnroll/%1_%3%4.crt"

# set ca time limits
info "Setting CA time limits"
certutil -setreg ca\ValidityPeriod "Years"
certutil -setreg ca\ValidityPeriodUnits 10

# CRL time limits
info "Setting CRL time limits"
certutil -setreg CA\CRLPeriodUnits 13
certutil -setreg CA\CRLPeriod "Weeks"
certutil -setreg CA\CRLDeltaPeriodUnits 0
certutil -setreg CA\CRLOverlapPeriodUnits 6
certutil -setreg CA\CRLOverlapPeriod "Hours"

# Enable auditing
certutil -setreg CA\AuditFilter 127

# Restart service
info "Restarting service 'certsvc'"
Restart-Service certsvc

# new crl
info "Adding new CRL"
certutil -crl

