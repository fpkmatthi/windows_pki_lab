# Provisioning script for win-cltxx

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname         = 'win-clt01',
    [string]$domain           = 'fpkmatthi.local',
    [string]$domain_user      = 'FPKMATTHI\MatthiasVDV',
    [string]$domain_user_pw   = 'Admin2019'
)

$PROVISIONING_SCRIPTS = "c:\vagrant\provisioning"

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Joining domain on $hostname"

join_domain $domain $domain_user $domain_user_pw
