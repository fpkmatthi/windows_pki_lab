# Provisioning script for win-cltxx

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname               = 'win-clt01',
    [string]$additional_local_admin = 'fpkmatthi\user'
)

$PROVISIONING_SCRIPTS = "c:\vagrant\provisioning"

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Adding local admin on $hostname"

Add-LocalGroupMember -Group Administrators -Member $additional_local_admin
