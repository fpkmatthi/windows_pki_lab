# Provisioning script for win-cltxx

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname         = 'win-clt01',
    [string]$lan_adapter_name = 'LAN'
)

$PROVISIONING_SCRIPTS = "c:\vagrant\provisioning"

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Fix adapter settings on $hostname"

set_adapter_dhcp $lan_adapter_name
