# Provisioning script for win-cltxx

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname         = 'win-clt01'
)
$PROVISIONING_SCRIPTS = "c:\vagrant\provisioning"

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Creating shares on $hostname"

New-Item -ItemType directory -Path "C:\Share"
New-SmbShare -Name "win10share" -Path "C:\Share" -FullAccess "Administrators" -ChangeAccess "Users"

