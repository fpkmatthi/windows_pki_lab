# Utility functions that are useful in all provisioning scripts.

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
# Set to 'yes' if debug messages should be printed.
param(
    [string]$debug_output = 'yes'
)

# Set to 'yes' if debug messages should be printed.
# New-Variable -Name debug_output -Value "yes" -Option Constant

#------------------------------------------------------------------------------
# Logging and debug output
#------------------------------------------------------------------------------
# {{{
# Usage: info [-info_text] [ARG]
#
# Prints argument on the standard output stream
Function info() {
    param([string]$info_text)

    Write-Host "### $info_text"
}

# Usage: debug [-debug_text] [ARG]
#
# Prints all arguments on the standard error stream
Function debug() {
    param([string]$debug_text)

    if ( $debug_output -eq "yes" ){
        Write-Host ">>> $debug_text"
    }
}

# Usage: error [-error_text] [ARG]
#
# Prints all arguments on the standard error stream
Function error() {
    param([string]$error_text)

    Write-Warning $error_text
}
# }}}

#------------------------------------------------------------------------------
# Helper Functions
#------------------------------------------------------------------------------
# {{{
function join_domain() {
    param(
        [string]$domain,
        [string]$domain_user,
        [string]$password
    )

    $secure_pw = ConvertTo-SecureString $password -AsPlainText -Force
    $creds = New-Object System.Management.Automation.PsCredential($domain_user,$secure_pw) 

    debug "Joining domain $domain with user $domain_user"
    Add-computer -DomainName $domain -DomainCredential $creds -Verbose
}

function rename_adapter() {
    param(
        [string]$current_name,
        [string]$new_name
    )
    # if ("$current_adapter_name" -ne "$new_name") {
    if ("$current_name" -ne "$new_name") {
        debug "Renaming adapter $current_name to $new_name"
        (Get-NetAdapter -Name $current_name 2> $null) | Rename-NetAdapter -NewName $new_name # 2> $null
    }
}

function disable_adapter() {
    param()
    # TODO
}

function set_static_ip() {
    param(
        [string]$adapter,
        [string]$ipv4_address,
        [int]$prefix,
        [string]$default_gateway
    )

    ### Remove static ip by setting it to dhcp and removing the default gateway
    ###     We have to remove the default gateway manually because it is grayed
    ###     out but stil persists
    debug "Removing old IP settings"
    Set-NetIPInterface -InterfaceAlias "$adapter" -Dhcp Enabled 2> $null
    Get-NetIPAddress -InterfaceAlias "$adapter" | Remove-NetRoute -Confirm:$false 2> $null

    debug "Setting adapter IP of $adapter to $ipv4_address/$prefix"
    if ("$default_gateway") {
        debug "Setting default gateway to $default_gateway"
        New-NetIPAddress -InterfaceAlias "$adapter" -IPAddress "$ipv4_address" -PrefixLength $prefix -DefaultGateway "$default_gateway" -AddressFamily IPv4 > $null
    } else {
        New-NetIPAddress -InterfaceAlias "$adapter" -IPAddress "$ipv4_address" -PrefixLength $prefix > $null
    }
}

function set_static_dns() {
    param(
        [string]$adapter,
        [string]$primary_dns,
        [string]$secondary_dns
    )

    debug "Setting static DNS of adapter $adapter to $primary_dns and $secondary_dns"
    if ($secondary_dns) {
        Set-DnsClientServerAddress -InterfaceAlias $adapter -ServerAddresses($primary_dns,$secondary_dns)
    } else {
        Set-DnsClientServerAddress -InterfaceAlias $adapter -ServerAddresses $primary_dns
    }
}

function set_adapter_dhcp() {
    param(
        [string]$adapter
    )

    debug "Setting adapter $adapter to dhcp"
    Set-DnsClientServerAddress -InterfaceAlias $adapter -ResetServerAddresses
}

function disable_ipv6() {
    param(
        [string]$adapter
    )
    Disable-NetAdapterBinding -Name $adapter -ComponentID ms_tcpip6
}

function turn_on_network_discovery() {
    # Turn on these services
    #   - DNS Client
    #   - Function Discovery Resource Publication
    #   - SSDP Discovery
    #   - UPnP Device Host
    debug "Turning on Network Discovery"
    Set-Service -Name "FDResPub" -StartupType "Automatic"
    Start-Service -DisplayName "Function Discovery Resource Publication" 

    Set-Service -Name "Dnscache" -StartupType "Automatic" 2> $null
    Start-Service -DisplayName "DNS Client" 

    Set-Service -Name "SSDPSRV" -StartupType "Automatic"
    Start-Service -DisplayName "SSDP Discovery" 

    Set-Service -Name "upnphost" -StartupType "Automatic"
    Start-Service -DisplayName "UPnP Device Host" 

    # Check firewall rules
    # Set action to allow
    Get-NetFirewallRule -DisplayGroup "Network Discovery" | Set-NetFirewallRule -Action Allow
    # Enabling the rule
    Get-NetFirewallRule -DisplayGroup "Network Discovery" | Enable-NetFirewallRule

    # Set action to allow
    Get-NetFirewallRule -DisplayGroup "File and Printer Sharing" | Set-NetFirewallRule -Action Allow 
    # Enabling the rule
    Get-NetFirewallRule -DisplayGroup "File and Printer Sharing" | Enable-NetFirewallRule


    # Turn on network discovery
    netsh advfirewall firewall set rule group="Network Discovery" new enable=Yes
}

function mount_image_and_go_to_drive() {
    param(
        [string]$src,
        [string]$dest
    )
    debug "Checking if ISO present"
    if (!(Test-Path $dest)) {
        if (Test-Path("$src")) {
            # copy to downloads
            debug 'Copying from shared folder'
            Copy-Item $src -Destination $dest
        } else {
            debug "No ISO present in synced folder (skipping)"
        }
    } else {
        debug "ISO already copied (skipping)"
    }

    $isoDrive = (Get-DiskImage -ImagePath $dest | Get-Volume)
    if (!($isoDrive)) {
        debug 'Mounting iso'
        Mount-DiskImage -ImagePath $dest
        $isoDrive = (Get-DiskImage -ImagePath $dest | Get-Volume).DriveLetter
    } else {
        debug 'ISO already mounted (skipping)'
    }
    $isoDrive = (Get-DiskImage -ImagePath $dest | Get-Volume).DriveLetter
    debug "Driveletter: $isoDrive"

    cd "${isoDrive}:"
    # cd "${isoDrive}"
}
# }}}

#------------------------------------------------------------------------------
# ADDS
#------------------------------------------------------------------------------
# {{{
function install_ADDS() {
    $is_AD_domainservices_installed=(Get-WindowsFeature AD-Domain-Services).Installed
    if ("$is_AD_domainservices_installed" -eq 'False') {
        debug "Installing AD-Domain-Services"
        Install-WindowsFeature AD-Domain-Services
    }

    $is_RSAT_admincenter_installed=(Get-WindowsFeature RSAT-AD-AdminCenter).Installed
    if ("$is_RSAT_admincenter_installed" -eq 'False') {
        debug "Installing RSAT-AD-AdminCenter"
        Install-WindowsFeature RSAT-AD-AdminCenter
    }

    $is_RSAT_addstools_installed=(Get-WindowsFeature RSAT-ADDS-Tools).Installed
    if ("$is_RSAT_addstools_installed" -eq 'False') {
        debug "Installing RSAT-ADDS-Tools"
        Install-WindowsFeature RSAT-ADDS-Tools
    }
}

function add_new_dc_new_forest() {
    param(
        [string]$domain,
        [string]$netbios,
        [string]$domain_admin_pw
    )
    $secure_domain_admin_pw = ConvertTo-SecureString $domain_admin_pw -AsPlainText -Force

    $domaincontroller_installed=(Get-ADDomainController 2> $null)
    if (!"$domaincontroller_installed") {
        debug "Installing AD forest (+-30min)"
        Import-Module ADDSDeployment
        # windows 2016 is oldest in forest so forest value 7, I guess
        Install-ADDSForest -InstallDns -CreateDnsDelegation:$False -ForestMode 7 -DomainMode 7 -DomainName $domain -SafeModeAdministratorPassword $secure_domain_admin_pw -NoRebootOnCompletion -Force
    }
}

function add_new_dc_existing_forest() {
    param(
        [string]$domain,
        [string]$netbios,
        [string]$admin_pw,
        [string]$dc1_hostname
    )
    $secure_admin_pw = ConvertTo-SecureString $admin_pw -AsPlainText -Force

    $domaincontroller_installed=(Get-ADDomainController 2> $null)
    if (!"$domaincontroller_installed") {
        debug 'Installing AD forest (+-30min)'
        Import-Module ADDSDeployment

        $creds = New-Object System.Management.Automation.PSCredential ("$netbios\Administrator",$secure_admin_pw)

        install-ADDSDomainController -DomainName $domain -ReplicationSourceDC "$dc1_hostname.$domain" -credential $creds -InstallDns -createDNSDelegation:$false -NoRebootOnCompletion -SafeModeAdministratorPassword $secure_admin_pw -Force
    }
}

function install_DHCP() {
    $is_DHCP_installed=(Get-WindowsFeature DHCP).Installed
    if ("$is_DHCP_installed" -eq 'False') {
        Install-WindowsFeature DHCP -IncludeManagementTools
    }
}

function add_dns_record() {
    param(
        [string]$record_name,
        [string]$record_zone_name,
        [string]$record_type,
        [string]$ipaddress,
        # [string]$record_mx_name,
        # [string]$record_cname_name,
        [string]$record_mail_exchange,
        [int]$record_preference,
        [string]$record_hostname_alias
    )

    # TODO: test with and without "2> $null"
    $record_exists=(Get-DnsServerResourceRecord -Name $record_name -RRType $record_type -ZoneName $record_zone_name 2> $null)

    if(!$record_exists) {
        # make record
        if($record_type -eq "MX") {
            Add-DnsServerResourceRecordMX -Name $record_name -ZoneName $record_zone_name -MailExchange $record_mail_exchange -Preference $record_preference
        } elseif ($record_type -eq "CNAME") {
            Add-DnsServerResourceRecordCName -Name $record_name -ZoneName $record_zone_name -HostNameAlias $record_hostname_alias
        } elseif ($record_type -eq "A") {
            Add-DnsServerResourceRecordA -Name $record_name -ZoneName $record_zone_name -IPv4Address $ipaddress
        }
    } else {
        Write-Warning "Record $record_name already exists (skipping)"
    }

}
# }}}

# -----------------------------------------------------------------------------
# Active Directory OU
# -----------------------------------------------------------------------------
# {{{
function add_ou() {
    param(
       [string]$ou_name,
       [string]$ou_description
    )
    debug "Adding OU $ou_name"
    New-ADOrganizationalUnit -Name $ou_name -Description $ou_description
}

function add_group() {
    param(
       [string]$groupname,
       [string]$display_name
       # [string]$path
    )
    debug "Adding group $groupname"
    New-ADGroup -Name $groupname -DisplayName $display_name -Path "OU=$groupname,DC=fpkmatthi,DC=local" -GroupCategory Security -GroupScope Global
}

function add_user() {
    param(
        [string]$name,
        [string]$surname,
        [string]$sambaname,
        [string]$department,
        [string]$description,
        [string]$displayname,
        [string]$givenname,
        [string]$state,
        [string]$city,
        [string]$postalcode,
        [string]$email,
        [string]$office,
        [string]$employeeid,
        [string]$homephone,
        [string]$initials,
        [string]$groupname,
        [string]$password
    )

    $secure_pw = ConvertTo-SecureString $password -AsPlainText -Force

    debug "Adding user $name $surname ($sambaname)"
    New-AdUser -Name $name -Surname $surname -SamAccountName $sambaname -Department $department -Description $description -DisplayName $displayname -GivenName $givenname -State $state -City $city -PostalCode $postalcode -EmailAddress $email -Office $office -EmployeeID $employeeid -HomePhone $homephone -Initials $initials -Path "OU=$groupname,DC=fpkmatthi,DC=local" -AccountPassword $secure_pw
}

function add_user_to_group() {
    param(
       [string]$username,
       [string]$groupname
    )
    debug "Adding $username to $groupname"
    Add-ADGroupMember -Identity "CN=$groupname,OU=$groupname,DC=fpkmatthi,DC=local" -Members "CN=$username,OU=$groupname,DC=fpkmatthi,DC=local"
}

function add_manager_to_group() {
    param(
       [string]$username,
       [string]$groupname
    )
    debug "Assigning $username as manager of $groupname"
    Set-ADGroup -Identity "CN=$groupname,OU=$groupname,DC=fpkmatthi,DC=local" -ManagedBy "CN=$username,OU=$groupname,DC=fpkmatthi,DC=local"
}

function add_manager_to_ou() {
    param(
       [string]$username,
       [string]$groupname
    )
    debug "Adding $username as manager to ou $groupname"
    Set-ADOrganizationalUnit -Identity "OU=$groupname,DC=fpkmatthi,DC=local" -ManagedBy "CN=$username,OU=$groupname,DC=fpkmatthi,DC=local"
}

function unlock_user() {
    param(
       [string]$username,
       [string]$groupname
    )
    debug "Unlocking useraccount $username from group $groupname"
    Enable-ADAccount -Identity "CN=$username,OU=$groupname,DC=fpkmatthi,DC=local"
}

function get_ad_summary() {
    echo 'vagrant Group Membership'
    Get-ADPrincipalGroupMembership -Identity 'vagrant' | Select-Object Name,DistinguishedName,SID | Format-Table -AutoSize | Out-String -Width 2000

    echo 'Domain Administrators'
    Get-ADGroupMember -Identity 'Domain Admins' | Select-Object Name,DistinguishedName,SID | Format-Table -AutoSize | Out-String -Width 2000

    echo 'Enabled Domain User Accounts'
    Get-ADUser -Filter {Enabled -eq $true} | Select-Object Name,DistinguishedName,SID | Format-Table -AutoSize | Out-String -Width 2000 
}
# }}}

# -----------------------------------------------------------------------------
# Exchange
# -----------------------------------------------------------------------------
# {{{
# }}}

# -----------------------------------------------------------------------------
# Sharepoint
# -----------------------------------------------------------------------------
# {{{
# }}}

# -----------------------------------------------------------------------------
# SQL
# -----------------------------------------------------------------------------
# {{{
function get_computermemory() {
    $mem = Get-WMIObject -class Win32_PhysicalMemory | Measure-Object -Property Capacity -Sum
    return ($mem.Sum / 1MB)
}

function get_sqlmaxmemory() { 
    # $memtotal = get_computermemory
    $memtotal = 8192
    $min_os_mem = 2048
    if ($memtotal -le $min_os_mem) {
        Return $null
    }
    if ($memtotal -ge 8192) {
        $sql_mem = $memtotal - 2048
    } else {
        $sql_mem = $memtotal * 0.8
    }
    return [int]$sql_mem
}

function set_sqlinstance_memory() {
    param(
        [Microsoft.SQLServer.Management.Smo.Server]$server,
        [int]$maxmem = $null, 
        [int]$minmem = 0
    )
 
    if ($minmem -eq 0) {
        $minmem = $maxmem
    }

    if ($server.status) {
        debug "Maximum Memory is now: $($server.Configuration.MaxServerMemory.RunValue)"
        debug "Minimum Memory is now: $($server.Configuration.MinServerMemory.RunValue)"
 
        debug "Setting Maximum Memory to: $maxmem"
        debug "Setting Minimum Memory to: $minmem"
        $server.Configuration.MaxServerMemory.ConfigValue = $maxmem
        $server.Configuration.MinServerMemory.ConfigValue = $minmem   
        $server.Configuration.Alter()
    }
}

function set_sqluser_permission() {
    param(
        [Microsoft.SQLServer.Management.Smo.Server]$server,
        [string]$sqlusername,
        [string]$role,
        [switch]$create
    )
    $login = New-Object Microsoft.SqlServer.Management.Smo.Login $server, "$sqlusername"
    $login.LoginType = [Microsoft.SqlServer.Management.Smo.LoginType]::WindowsUser
    # To create a non-existing user
    if ($create) {
        $login.Create()
    }
    $login.AddToRole($role)
}
# }}}

# -----------------------------------------------------------------------------
# SCCM
# -----------------------------------------------------------------------------
# {{{
function disable_lockscreen() {
    powercfg -change -monitor-timeout-ac 0
    powercfg -change -monitor-timeout-dc 0
}

function integrateMDT{
    $partdeploy = 'C:'
    $Folder = "$partdeploy\DeploymentShare"
    $Share  = "DeploymentShare$"

    Import-Module "C:\Program Files\Microsoft Deployment Toolkit\bin\MicrosoftDeploymentToolkit.psd1"

    New-Item -Path $Folder -Type Directory
    New-SmbShare -Name $Share -Path $Folder -FullAccess EVERYONE

    New-PSDrive -Name "DS001" -PSProvider "MDTProvider" -Root $Folder -NetworkPath "\\$ENV:COMPUTERNAME\$Share" -Description "Deployment Share" | Add-MDTPersistentDrive

    $os = Import-MDTOperatingSystem -Path "DS001:\Operating Systems" -SourcePath "$partdeploy\" -DestinationFolder "Windows 10"

    Import-MDTTaskSequence -Path "DS001:\Task Sequences" -Name "Windows 10" -Template "Client.xml" -Comments "Build and Capture Windows 10" -ID "WIN10" -Version "1.0" -OperatingSystemPath "DS001:\Operating Systems\$($os.Name)" -FullName $Domain -OrgName $Domain -AdminPassword $pswd

    Update-MDTDeploymentShare -Path "DS001:"

    $File = "$Folder\Control\CustomSettings.ini"
    (Get-Content $file | Select-String -Pattern 'SkipCapture' -NotMatch) | Set-Content $file

    Start-Process "C:\Program Files\Microsoft Deployment Toolkit\Bin\Microsoft.BDD.WizLauncher.exe" ConfigureSCCM -wait


    $MDT  = "C:\Program Files\Microsoft Deployment Toolkit"
    $SCCM = "C:\Program Files (x86)\Microsoft Configuration Manager\AdminConsole"
    $MOF  = "$SCCM\Bin\Microsoft.BDD.CM12Actions.mof"

    Copy-Item "$MDT\Bin\Microsoft.BDD.CM12Actions.dll" "$SCCM\Bin\Microsoft.BDD.CM12Actions.dll"
    Copy-Item "$MDT\Bin\Microsoft.BDD.Workbench.dll" "$SCCM\Bin\Microsoft.BDD.Workbench.dll"
    Copy-Item "$MDT\Bin\Microsoft.BDD.ConfigManager.dll" "$SCCM\Bin\Microsoft.BDD.ConfigManager.dll"
    Copy-Item "$MDT\Bin\Microsoft.BDD.CM12Wizards.dll" "$SCCM\Bin\Microsoft.BDD.CM12Wizards.dll"
    Copy-Item "$MDT\Bin\Microsoft.BDD.PSSnapIn.dll" "$SCCM\Bin\Microsoft.BDD.PSSnapIn.dll"
    Copy-Item "$MDT\Bin\Microsoft.BDD.Core.dll" "$SCCM\Bin\Microsoft.BDD.Core.dll"
    Copy-Item "$MDT\SCCM\Microsoft.BDD.CM12Actions.mof" $MOF
    Copy-Item "$MDT\Templates\CM12Extensions\*" "$SCCM\XmlStorage\Extensions\" -Force -Recurse
    (Get-Content $MOF).Replace('%SMSSERVER%', $SiteServer).Replace('%SMSSITECODE%', $SiteCode) | Set-Content $MOF
    Start-Process "C:\Windows\System32\wbem\mofcomp.exe" "$SCCM\Bin\Microsoft.BDD.CM12Actions.mof" -wait

    New-ItemProperty -Path HKLM:Software -Name CaptureTime -Value $(Get-Date -UFormat "%Y/%m/%d")

    $app = Import-MDTApplication `
      -Enable $true `
      -Path "DS001:\Applications" `
      -Name "Branding" `
      -ShortName "Branding" `
      -DisplayName "Branding" `
      -CommandLine "powershell.exe -ExecutionPolicy Bypass -File Configure.ps1" `
      -WorkingDirectory ".\Applications\Branding" `
      -ApplicationSourcePath "C:\Media\Branding" `
      -DestinationFolder "Branding"
     
  $step = [xml]@"
<step type="BDD_InstallApplication" name="Branding" description="" disable="false" continueOnError="false" runIn="WinPEandFullOS" successCodeList="0 3010">  
<defaultVarList>  
  <variable name="ApplicationGUID" property="ApplicationGUID">$($x.GUID)</variable>
  <variable name="ApplicationSuccessCodes" property="ApplicationSuccessCodes">0 3010</variable>
</defaultVarList>  
<action>cscript.exe "%SCRIPTROOT%\ZTIApplications.wsf"</action>  
</step>  
"@

    $ts    = "C:\DeploymentShare\Control\WIN10\ts.xml"
    $xml   = [xml](Get-Content $ts)
    $node  = $xml.ImportNode($step.get_DocumentElement(), $true)
    $tasks = $xml.Sequence.Group.Group | ? Name -eq "Custom Tasks"
    $tasks.AppendChild($node)
    $xml.Save($ts)

    Import-Module "C:\Program Files\Microsoft Deployment Toolkit\bin\MicrosoftDeploymentToolkit.psd1"
    Restore-MDTPersistentDrive

    $settings = "C:\DeploymentShare\Control\Settings.xml"
    $xml = [xml](Get-Content $settings)
    $xml.Settings.Item("Boot.x86.FeaturePacks")."#text" = "winpe-mdac,winpe-netfx,winpe-powershell"
    $xml.Settings.Item("Boot.x64.FeaturePacks")."#text" = "winpe-mdac,winpe-netfx,winpe-powershell"
    $xml.Save($settings)

    Update-MDTDeploymentShare -Path "DS001:" -Force

    # This example only updates the x64 boot WIM.
    mkdir C:\Mount

    $adk = "C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit"

    dism /Mount-Wim /WimFile:C:\DeploymentShare\Boot\LiteTouchPE_x64.wim /MountDir:C:\Mount /Index:1

    Start-Process "$adk\Deployment Tools\amd64\DISM\dism.exe" /Add-Package /Image:C:\Mount /PackagePath:"$adk\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-NetFx.cab" -wait
    Start-Process "$adk\Deployment Tools\amd64\DISM\dism.exe" /Add-Package /Image:C:\Mount /PackagePath:"$adk\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-NetFx_en-us.cab" -wait

    Start-Process "$adk\Deployment Tools\amd64\DISM\dism.exe" /Add-Package /Image:C:\Mount /PackagePath:"$adk\Windows Preinstallation Environment\amd64\WinPE_OCs\WinPE-PowerShell.cab" -wait
    Start-Process "$adk\Deployment Tools\amd64\DISM\dism.exe" /Add-Package /Image:C:\Mount /PackagePath:"$adk\Windows Preinstallation Environment\amd64\WinPE_OCs\en-us\WinPE-PowerShell_en-us.cab" -wait

    dism /UnMount-Wim /MountDir:C:\Mount /Commit
    rmdir C:\Mount
}
# }}}
