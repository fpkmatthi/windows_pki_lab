# Provisioning script for win-webserv01

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname        = 'win-webserv01',
    [string]$domain          = 'fpkmatthi.local',
    [string]$netbios         = 'FPKMATTHI',
    [string]$local_admin_pw  = 'LocalAdmin2019',
    [string]$domain_admin_pw = 'DomainAdmin2019'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"
$secure_local_admin_pw  = ConvertTo-SecureString $local_admin_pw -AsPlainText -Force
$secure_domain_admin_pw = ConvertTo-SecureString $domain_admin_pw -AsPlainText -Force

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting domain controller installation on $hostname"

info "Installing Web server (IIS)"
Install-WindowsFeature Web-WebServer -IncludeManagementTools

info "Creating new share"
mkdir C:\CertEnroll 
New-smbshare -Name "CertEnroll" -Path "C:\CertEnroll" -FullAccess SYSTEM,"FPKMATTHI\Domain Admins" -ChangeAccess "FPKMATTHI\Cert Publishers"

info "Creating new virtual web dir"
New-WebVirtualdirectory -Site "Default Web Site" -PhysicalPath "C:\CertEnroll" -Name "CertEnroll"

info "Enable double escaping"
C:\Windows\system32\inetsrv\appcmd.exe set config "Default Web Site" /section:system.webServer/Security/requestFiltering -allowDoubleEscaping:True

info "Enabling directory browsing"
# Get-WebConfigurationProperty -filter /system.webServer/directoryBrowse -name enabled -PSPath 'IIS:\Sites\Default Web Site'
C:\Windows\system32\inetsrv\appcmd.exe set config "Default Web Site" /section:directoryBrowse /enabled:true

iisreset

info "Copying crt and crl"
# TODO: test this
# $creds = New-Object System.Management.Automation.PsCredential("$netbios\Administrator",$secure_domain_admin_pw) 
New-PSDrive -Name winrootca -PSProvider FileSystem -Root '\\WIN-ROOT-CA\C$\Windows\System32\CertSrv\CertEnroll' | ForEach-Object { Set-Location "$_`:" }
Copy ./* C:\CertEnroll

