# Provisioning script for win-issuing-ca

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname        = 'win-issuing-ca'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting post config installation on $hostname"

# Create credential

Certutil -installcert "C:\win-issuing-ca.fpkmatthi.local_fpkmatthi_Issuing CA.crt"

start-service certsvc

info "Adding CRL distribution point"
certutil -setreg CA\CRLPublicationURLs "1:C:\Windows\system32\CertSrv\CertEnroll\%3%8%9.crl\n10:ldap:///CN=%7%8,CN=%2,CN=CDP,CN=Public Key Services,CN=Services,%6%10\n2:http://crt.fpkmatthi.local/CertEnroll/%3%8%9.crl"
 
info "Adding AIA distribution point"
certutil -setreg CA\CACertPublicationURLs "1:C:\Windows\system32\CertSrv\CertEnroll\%1_%3%4.crt\n2:ldap:///CN=%7,CN=AIA,CN=Public Key Services,CN=Services,%6%11\n2:http://crt.fpkmatthi.local/CertEnroll/%1_%3%4.crt"

## ca and crl time limits
certutil -setreg CA\CRLPeriodUnits 7
certutil -setreg CA\CRLPeriod "Days"
certutil -setreg CA\CRLOverlapPeriodUnits 3
certutil -setreg CA\CRLOverlapPeriod "Days"
certutil -setreg CA\CRLDeltaPeriodUnits 0
certutil -setreg ca\ValidityPeriodUnits 3
certutil -setreg ca\ValidityPeriod "Years"

# Enable auditing
certutil -setreg CA\AuditFilter 127

#Restart-Service certsvc
info "Restartig service 'certsvc'"
Restart-Service certsvc

info "Adding new CRL"
certutil -crl

##TODO: run PKIView.msc to view config
##TODO: can now turn off root CA

Copy "C:\Windows\System32\CertSrv\CertEnroll\win-issuing-ca.fpkmatthi.local_fpkmatthi Issuing CA.crt" \\crt.fpkmatthi.local\CertEnroll\
Copy "C:\Windows\System32\CertSrv\CertEnroll\fpkmatthi Issuing CA.crl" \\crt.fpkmatthi.local\CertEnroll\

