# Provisioning script for win-issuing-ca

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname        = 'win-issuing-ca',
    [string]$domain          = 'fpkmatthi.local',
    [string]$netbios         = 'FPKMATTHI',
    [string]$local_admin_pw  = 'LocalAdmin2019',
    [string]$domain_admin_pw = 'DomainAdmin2019'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"
$secure_local_admin_pw  = ConvertTo-SecureString $local_admin_pw -AsPlainText -Force
$secure_domain_admin_pw = ConvertTo-SecureString $domain_admin_pw -AsPlainText -Force

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting CA installation on $hostname"

# TODO: use variables
# Create credential
$creds = New-Object System.Management.Automation.PsCredential("$netbios\Administrator",$secure_domain_admin_pw) 

# install ca stuff
Install-WindowsFeature ADCS-Cert-Authority -IncludeManagementTools
Install-ADcsCertificationAuthority -CACommonName "fpkmatthi Issuing CA" -CAType EnterpriseSubordinateCA -CryptoProviderName "RSA#Microsoft Software Key Storage Provider" -HashAlgorithmName SHA256 -KeyLength 2048 -Credential $creds -Force

Install-WindowsFeature ADCS-web-enrollment
Install-ADCSwebenrollment -Credential $creds -Force

$vagrant_pw = ConvertTo-SecureString "vagrant" -AsPlainText -Force
$vagrant_creds = New-Object System.Management.Automation.PsCredential("WIN-ROOT-CA\vagrant",$vagrant_pw) 
New-PSDrive -Name winrootca -PSProvider FileSystem -Root '\\WIN-ROOT-CA\C$' -Credential $vagrant_creds | ForEach-Object { Set-Location "$_`:" }
Copy "C:\win-issuing-ca.fpkmatthi.local_fpkmatthi Issuing CA.req" ./
Copy "./Windows/System32/CertSrv/CertEnroll/*" C:\

# New-PSDrive -Name winwebserv -PSProvider FileSystem -Root '\\WIN-WEBSERV01\CertEnroll' -Credential $creds | ForEach-Object { Set-Location "$_`:" }
# Copy ./* C:\

# publish cert
certutil -f -dspublish "C:\win-root-ca_fpkmatthi Root CA.crt" RootCA
certutil -f -dspublish "C:\fpkmatthi Root CA.crl"

certutil -addstore -f root "C:\win-root-ca_fpkmatthi Root CA.crt"
certutil -addstore -f root "C:\fpkmatthi Root CA.crl"

