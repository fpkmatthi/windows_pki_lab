# General provisioning script

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname         = 'win-dc1',
    [string]$internal_ipv4    = '192.168.56.10',
    [int]$internal_prefix     = 24,
    [string]$wan_adapter_name = 'WAN',
    [string]$lan_adapter_name = 'LAN',
    [string]$default_gateway  = '192.168.56.1',
    [string]$primary_dns      = '192.168.56.20',
    [string]$secondary_dns    = '192.168.56.21',
    [string]$country          = 'eng-BE',
    [string]$timezone         = 'Romance Standard Time',
    [string]$local_admin_pw   = 'LocalAdmin2019',
    [string]$domain_admin_pw  = 'DomainAdmin2019'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"
$secure_local_admin_pw  = ConvertTo-SecureString $local_admin_pw -AsPlainText -Force
$secure_domain_admin_pw = ConvertTo-SecureString $domain_admin_pw -AsPlainText -Force

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Changing Locale settings"
Set-Culture -CultureInfo $country
Set-Timezone -Name $timezone

info "Configuring local admin account and password"
Set-LocalUser -Name Administrator -AccountNeverExpires -Password $secure_local_admin_pw -PasswordNeverExpires:$true -UserMayChangePassword:$true

info "Changing adapter settings on $hostname"
$adaptercount=(Get-NetAdapter | measure).count
if ($adaptercount -eq 1) {
    rename_adapter "Ethernet" $wan_adapter_name
} 
elseif ($adaptercount -eq 2) {
    rename_adapter "Ethernet" $wan_adapter_name
    rename_adapter "Ethernet 2" $lan_adapter_name
}

### NOTE: set default gateway as LAN ipv4 for the NAT router service
set_static_ip $lan_adapter_name $internal_ipv4 $internal_prefix $default_gateway
set_static_dns $lan_adapter_name $primary_dns $secondary_dns
set_adapter_dhcp $wan_adapter_name
turn_on_network_discovery

