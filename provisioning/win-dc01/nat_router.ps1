# Provisioning script for win-dc01

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname         = 'win-dc01',
    [string]$lan_adapter_name = 'LAN',
    [string]$wan_adapter_name = 'WAN'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting NAT routing configuration on $hostname"

Install-WindowsFeature Routing -IncludeManagementTools -IncludeAllSubFeature

Install-RemoteAccess -VpnType Vpn

Get-NetAdapter | Set-NetIPInterface -Forwarding Enabled

cmd.exe /c "netsh routing ip nat install"
cmd.exe /c "netsh routing ip nat add interface $wan_adapter_name"
cmd.exe /c "netsh routing ip nat set interface $wan_adapter_name mode=full"
cmd.exe /c "netsh routing ip nat add interface $lan_adapter_name"
