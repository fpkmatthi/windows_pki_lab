# Provisioning script for win-dc01

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname         = 'win-dc01',
    [string]$domain           = 'fpkmatthi.local',
    [string]$internal_ipv4    = '192.168.56.10',
    [string]$default_gw       = '192.168.56.20',
    [string]$primary_dns      = '192.168.56.20',
    [string]$secondary_dns    = '192.168.56.21',
    [string]$dhcp_start_range = '192.168.56.150',
    [string]$dhcp_end_range   = '192.168.56.200'
)
$PROVISIONING_SCRIPTS = "c:\vagrant\provisioning"

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting DHCP configuration on $hostname"

#debug 'Installing DHCP'
$is_DHCP_installed=(Get-WindowsFeature DHCP).Installed
if ("$is_DHCP_installed" -eq 'False') {
    Install-WindowsFeature DHCP -IncludeManagementTools
}

#debug 'Add DHCP to DC'
Add-DhcpServerInDC -DnsName "$hostname.$domain" -IpAddress $internal_ipv4

#debug 'Install DHCP Scope'
Add-DhcpServerV4Scope -Name 'DHCP Scope' -StartRange $dhcp_start_range -EndRange $dhcp_end_range -SubnetMask 255.255.255.0 -State Active

#debug 'Set option 66 and 67'
Set-DhcpServerv4OptionValue -ScopeId $dhcp_start_range -OptionId 066 -Value "WIN-SQL-SCCM.$domain"
Set-DhcpServerv4OptionValue -ScopeId $dhcp_start_range -OptionId 067 -Value "\smsboot\x64\wdsnbp.com"

#debug 'Install DHCP Lease'
Set-DhcpServerv4Scope -ScopeId $dhcp_start_range -LeaseDuration 7.00:00:00

# debug 'Authorize DHCP to operate domain'
if (Test-Connection $secondary_dns -Count 1 -Quiet) {
    Set-DhcpServerV4OptionValue -ScopeId $dhcp_start_range -DnsDomain $domain -DnsServer $primary_dns,$secondary_dns -Router $default_gw
} else {
    Set-DhcpServerV4OptionValue -ScopeId $dhcp_start_range -DnsDomain $domain -DnsServer $primary_dns -Router $default_gw
}

#debug 'Restart DHCP'
Restart-service -Name dhcpserver

#debug 'Verify DHCP scope'
Get-DhcpServerv4Scope

#debug 'Verify DHCP in DC'
Get-DhcpServerInDC

### NOTE: turn on network discovery needs to be run as the domain admin
###       that is why I place it here since this scripts is to be executed
###       as a domain admin as well
turn_on_network_discovery
