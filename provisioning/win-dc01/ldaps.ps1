# Provisioning script for win-dc01
# Install and set up LDAPS for IPv6 relay attack with mitm6 & ntlmrelayx.py

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname        = 'win-dc01',
    [string]$domain          = 'fpkmatthi.local',
    [string]$netbios         = 'FPKMATTHI',
    [string]$domain_admin_pw = 'DomainAdmin2019'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"
$secure_domain_admin_pw = ConvertTo-SecureString $domain_admin_pw -AsPlainText -Force

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting LDAPS configuration on $hostname"

if ((Get-WindowsFeature ADCS-Cert-Authority).Installed -eq 'False') {
    debug "Installing ADCS-Cert-Authority"
    Install-WindowsFeature Adcs-Cert-Authority
}

$creds = New-Object System.Management.Automation.PSCredential ("$netbios\Administrator",$secure_domain_admin_pw)

try {
    Install-ADcsCertificationAuthority -Credential $creds -CAType EnterpriseRootCa -CACommonName "win-dc01-ca" -CADistinguishedNameSuffix "DC=fpkmatthi,DC=local" -CryptoProviderName "RSA#Microsoft Software Key Storage Provider" -KeyLength 2048 -HashAlgorithmName SHA256 -ValidityPeriod Years -ValidityPeriodUnits 99 -DatabaseDirectory "C:\windows\system32\certLog" -LogDirectory "c:\windows\system32\CertLog" -Force
} catch {
    error "Failed to (re-)install ADcsCertificationAuthority"
}
