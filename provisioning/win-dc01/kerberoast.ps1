# Provisioning script for win-dc01

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname = 'win-dc01',
    [string]$domain   = 'fpkmatthi.local'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting kerberoasting configuration on $hostname"

### Vulnerable Service Account (for kerberoasting)
$User = Get-ADUser -Filter {sAMAccountName -eq "vsakerb"}
If ($User -eq $Null) {
    add_user "vsa" "kerb" "vsakerb" "Sale" "Account to test kerberoasting, pw is Admin2019" "vsakerb" "vsa" "Oost-Vlaanderen" "Aalst" "9300" "vsa@fpkmatthi.local" "B0.015" "4753" "0444727201" "VKERB" "Verkoop" "Admin2019"
    add_user_to_group "vsa" "Verkoop" 2> $null
    add_user_to_group "vsa" "Administrators" 2> $null
    add_user_to_group "vsa" "Domain Admins" 2> $null
    add_user_to_group "vsa" "Domain Users" 2> $null
    add_user_to_group "vsa" "Enterprise Admins" 2> $null
    add_user_to_group "vsa" "Group Policy Creator Owners" 2> $null
    add_user_to_group "vsa" "Schema Admins" 2> $null
    add_manager_to_group "vsa" "Verkoop" 2> $null
    add_manager_to_ou "vsa" "Verkoop" 2> $null
}
unlock_user "vsa" "Verkoop"

# TODO: Account settings for kerberoasting
info "Setting spn"
setspn -a win-dc01/vsakerb.fpkmatthi.local:60111 FPKMATTHI\vsakerb

# Add shares
info "Adding shares"
If (!(Test-Path "C:\Shares\hackme")) {
    New-Item -ItemType directory -Path "C:\Shares\hackme"
}
If (!(Get-SMBShare -Name "hackme" -ea 0)) {
    New-SmbShare -Name "hackme" -Path "C:\Shares\hackme" -FullAccess "Administrators" -ChangeAccess "Users"
}
