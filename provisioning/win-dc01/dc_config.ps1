# Provisioning script for win-dc01

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname         = 'win-dc01',
    [string]$lan_adapter_name = 'LAN',
    [string]$wan_adapter_name = 'WAN',
    [string]$primary_dns      = '192.168.56.20',
    [string]$scondary_dns     = '192.168.56.21',
    [string]$local_admin_pw   = 'LocalAdmin2019',
    [string]$domain_admin_pw  = 'DomainAdmin2019'
)
$PROVISIONING_SCRIPTS   = "c:\vagrant\provisioning"
$secure_local_admin_pw  = ConvertTo-SecureString $local_admin_pw -AsPlainText -Force
$secure_domain_admin_pw = ConvertTo-SecureString $domain_admin_pw -AsPlainText -Force

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting domain controller configuration on $hostname"

# Wait until we can access the AD. this is needed to prevent errors like:
# Unable to find a default server with Active Directory Web Services running.
debug 'Waiting for Active Directory to come online'
while ($true) {
    try {
        Get-ADDomain | Out-Null
        break
    } catch {
        Start-Sleep -Seconds 10
    }
}

Import-Module ActiveDirectory

debug 'Gatering domain info'
$addomain    = Get-ADDomain
$domain      = $addomain.DNSRoot
$domaindn    = $addomain.DistinguishedName
$usersadpath = "CN=Users,$domaindn"
# $password    = ConvertTo-SecureString -AsPlainText "Admin2019" -Force

debug 'Disabling all user accounts, except the ones defined'
$enabledaccounts = @("vagrant","Administrator")
Get-ADUser -Filter {Enabled -eq $true} | Where-Object {$enabledaccounts -notcontains $_.Name} | Disable-ADAccount

debug 'Configuring domain admin password.'
Set-ADAccountPassword -Identity "CN=Administrator,$usersadpath" -Reset -NewPassword $secure_domain_admin_pw
Set-ADUser -Identity "CN=Administrator,$usersadpath" -PasswordNeverExpires $true

debug 'Add vagrant to domain admins'
Add-ADGroupMember -Identity 'Domain Admins' -Members "CN=vagrant,$usersAdPath"
Add-ADGroupMember -Identity 'Enterprise Admins' -Members "CN=vagrant,$usersAdPath"

# Groups
add_ou "Sales" "Sales ou" 2> $null
add_group "Sales" "Sales" 2> $null

### Matthias
add_user "Matthias" "Van De Velde" "MatthiasVDV" "Sale" "Account voor Matthias" "MatthiasVDV" "Matthias" "West-Vlaanderen" "Koksijde" "8670" "matthias@fpkmatthi.local" "B0.015" "4732" "0444727200" "MVDV" "Sales" "Admin2019" 2> $null
add_user_to_group "Matthias" "Sales" 2> $null
add_manager_to_group "Matthias" "Sales" 2> $null
add_manager_to_ou "Matthias" "Sales" 2> $null
unlock_user "Matthias" "Sales"

### Joke
add_user "Joke" "Van De Velde" "JokeVDV" "Sale" "Account voor Joke" "JokeVDV" "Joke" "West-Vlaanderen" "Koksijde" "8670" "joke@fpkmatthi.local" "B0.016" "4733" "0444727200" "JVDV" "Sales" "Admin2019" 2> $null
add_user_to_group "Joke" "Sales" 2> $null
add_user_to_group "Joke" "Administrators" 2> $null
add_user_to_group "Joke" "Domain Admins" 2> $null
add_user_to_group "Joke" "Domain Users" 2> $null
add_user_to_group "Joke" "Enterprise Admins" 2> $null
add_user_to_group "Joke" "Group Policy Creator Owners" 2> $null
add_user_to_group "Joke" "Schema Admins" 2> $null
unlock_user "Joke" "Sales"

# get_ad_summary

info "Fixing DNS settings after promote to DC"
set_static_dns $lan_adapter_name $primary_dns $secondary_dns
set_adapter_dhcp $wan_adapter_name 
