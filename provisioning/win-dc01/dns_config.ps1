# Provisioning script for WIN-DC1

#------------------------------------------------------------------------------
# Variables
#------------------------------------------------------------------------------
param(
    [string]$hostname          = 'win-dc01',
    [string]$domain            = 'fpkmatthi.local',
    [string]$ip_win_root_ca    = '192.168.56.22',
    [string]$ip_win_issuing_ca = '192.168.56.23'
)
$PROVISIONING_SCRIPTS = "c:\vagrant\provisioning"

#------------------------------------------------------------------------------
# "Imports"
#------------------------------------------------------------------------------
. "$PROVISIONING_SCRIPTS\util.ps1"

#------------------------------------------------------------------------------
# Provision server
#------------------------------------------------------------------------------
info "Starting DNS configuration on $hostname"

$primary_zone_exists=(Get-DnsServerZone -Name $domain)
if (!$primary_zone_exists) {
    debug 'Add primary DNS zone and enable replication'
    Add-DnsServerPrimaryZone -Name $domain -ReplicationScope "Domain" -DynamicUpdate "Secure"
    Set-DnsServerPrimaryZone -Name $domain -SecureSecondaries "TransferToZoneNameServer"
}

debug 'Add CNAME recors'
# TODO: make this modular
[string]$webserver_hostname="win-webserv01"
add_dns_record -record_name "crt" -record_zone_name $domain -record_type "CNAME" -record_hostname_alias "$webserver_hostname.$domain."





### -----
# debug 'Add A records'
# add_dns_record -record_name "crt" -record_zone_name $domain -record_type "A" -ipaddress $ip_win_root_ca
#
# debug 'Add MX and CNAME records'
# add_dns_record -record_name "WINEXCSHP" -record_zone_name $domain -record_type "MX" -record_mail_exchange "mail.$domain" -record_preference 100
# add_dns_record -record_name "owa" -record_zone_name $domain -record_type "CNAME" -record_hostname_alias "mail.$domain"
#
# debug 'Add reverse lookup zone'
### TODO: if (!exists) {add}
# Add-DnsServerPrimaryZone -NetworkID "192.168.100.0/24" -ReplicationScope "Domain" -DynamicUpdate "Secure"
